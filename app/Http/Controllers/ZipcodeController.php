<?php

namespace App\Http\Controllers;

use App\Zipcode;
use Illuminate\Http\Request;

class ZipcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function index()
        {
           
            $error ="Error 404: Not Found";
            
             return response()->json($error, 404);
        }
    
        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
       /* public function store(Request $request)
        {
            
        }*/

    /**
     * Display the specified resource.
     *
     * @param  \App\zipcode  $zipcode
     * @return \Illuminate\Http\Response
     */
    public function show(Zipcode $zipcode)
    {
         $data[] = [
            'zip_code' => $zipcode->d_codigo,
            'locality' => $zipcode->d_ciudad,
            'federal_entity' => [
                        'name' => $zipcode->d_estado,
                        'code' => "",
                    ],
            'settlements' => [[
                        'name' => $zipcode->d_asenta,
                        'zone_type' => $zipcode->d_zona,
                        'settlement_type' => [
                            'name' => $zipcode->d_tipo_asenta,
                        ]],
                    ],
            'municipality' => [
                        'name' => $zipcode->D_mnpio,
                       
                    ],
         ];
         
       return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\zipcode  $zipcode
     * @return \Illuminate\Http\Response
     */
    public function edit(zipcode $zipcode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\zipcode  $zipcode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, zipcode $zipcode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\zipcode  $zipcode
     * @return \Illuminate\Http\Response
     */
    public function destroy(zipcode $zipcode)
    {
        //
    }
}
