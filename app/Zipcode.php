<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zipcode extends Model
{
    protected $keyType = 'string';
    protected $primaryKey = 'd_codigo';
    
    public static function select($d_codigo)
    {
        return self::select('id','d_codigo')
            ->where('d_codigo',$d_codigo)->first();       
    }
    
}
