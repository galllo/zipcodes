<?php

use Illuminate\Database\Seeder;
ini_set('memory_limit', -1);

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::disableQueryLog();
        $this->call(AguascalientesSeeder::class);
        $this->call(BajacaliforniaSeeder::class);
        $this->call(BajacaliforniasurSeeder::class);
        $this->call(CampecheSeeder::class);
        $this->call(ChiapasSeeder::class);
        $this->call(ChihuhuaSeeder::class);
        $this->call(ChoahuilaSeeder::class);
        $this->call(ColimaSeeder::class);
        $this->call(DistritofederalSeeder::class);
        $this->call(DurangoSeeder::class);
        $this->call(EstadodemexicoSeeder::class);
        $this->call(GuanajuatoSeeder::class);
        $this->call(GuerreroSeeder::class);
        $this->call(HidalgoSeeder::class);
        $this->call(JaliscoSeeder::class);
        $this->call(MichoacanSeeder::class);
        $this->call(MorelosSeeder::class);
        $this->call(NayaritSeeder::class);
        $this->call(NuevoleonSeeder::class);
        $this->call(OaxacaSeeder::class);
        $this->call(PueblaSeeder::class);
        $this->call(QueretaroSeeder::class);
        $this->call(QuintanarooSeeder::class);
        $this->call(SanluispotosiSeeder::class);
        $this->call(SinaloaSeeder::class);
        $this->call(SonoraSeeder::class);
        $this->call(TabascoSeeder::class);
        $this->call(TamaulipasSeeder::class);
        $this->call(TlaxcalaSeeder::class);
        $this->call(VeracruzSeeder::class);
        $this->call(YucatanSeeder::class);
        $this->call(ZacatecasSeeder::class);
    }
}
