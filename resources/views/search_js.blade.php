<script>
            function search(){
            
                var url = $('#url').val();
                var zip_code_info;
                var buscar_zip_code = $.ajax({
                    type: 'GET',
                    url: url,
                    success:function(data){
                        
                        zip_code_info = JSON.stringify(data, undefined, 3);
                        document.getElementById("response").innerHTML = zip_code_info;
                        document.getElementById("response").style.display = "block"; 
                    
                    },
                     error: function (xhr, ajaxOptions, thrownError) {
                       
                       document.getElementById("response").innerHTML = "Error "+xhr.status+": "+thrownError; 
                       document.getElementById("response").style.display = "block"; 
                    },
                }); 
            }
</script>   