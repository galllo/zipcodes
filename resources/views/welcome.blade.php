<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Reto Técnico</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="{{ asset('public/css/estilos.css') }}" rel="stylesheet">
        <!-- Styles -->
        
    </head>
    <body>
        <div class="col-md-8 col-md-offset-2 position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    ZIP CODE
                </div>
                    
                <div class="api-info">
                    <table id="apiinfo">
                        <tr>
                          <th>Name</th>
                          <th>Type</th>
                          <th>Description</th>
                        </tr>
                        <tr>
                          <td>zip_code</td>
                          <td>String</td>
                          <td>Show the zip code</td>  
                        </tr>
                        <tr>
                          <td>locality</td>
                          <td>String</td>
                          <td>Shows the name of the locality of the zip code</td>  
                        </tr>
                        <tr>
                          <td>federal_entity</td>
                          <td>Object</td>
                          <td></td>  
                        </tr>
                        <tr>
                          <td>name</td>
                          <td>String</td>
                          <td>Shows the name of the federal entity of the zip code</td>  
                        </tr>
                        <tr>
                          <td>code</td>
                          <td>String</td>
                          <td>Shows the code of the federal entity of the zip code</td>  
                        </tr>
                        <tr>
                          <td>settlements</td>
                          <td>Array</td>
                          <td></td>  
                        </tr>
                        <tr>
                          <td>name</td>
                          <td>String</td>
                          <td>Shows the name of the settlement of the zip code</td>  
                        </tr>
                        <tr>
                          <td>zone_type</td>
                          <td>String</td>
                          <td>Shows the zone type of the settlement of the zip code</td>  
                        </tr>
                        <tr>
                          <td>settlement_type</td>
                          <td>Object</td>
                          <td></td>  
                        </tr>
                        <tr>
                          <td>name</td>
                          <td>String</td>
                          <td>Shows the name of the settlement type of the zip code</td>  
                        </tr>      
                      </table>
                </div>    
                    <br>
                <div class="input-group">
                 <input id="url" type="text" class="form-control" name="url" value="http://localhost:8888/zipcodes/api/zipcodes/{zip_code}">
                 <span class="input-group-addon">url</span>
                </div>
                <br>
                <button type="button" class="btn btn-primary" onclick="search()">Enviar</button>
                    
                <pre id="response" class="response"></pre>    
            </div>
        </div>
    </body>
</html>
@include('search_js')